<?php declare(strict_types=1);


final class Notifications
{
    private $notifications = [];

    public function __construct($notificationsFilepath)
    {
        $this->loadFile($notificationsFilepath);
    }

    public function getNowNotificationBody() {
        $timezone = date_default_timezone_get();
        date_default_timezone_set('Europe/Kiev');
        $nowTime = date('H:i');
        date_default_timezone_set($timezone);

        if (!empty($this->notifications[$nowTime])) {
            return $this->notifications[$nowTime];
        } else {
            return false;
        }
    }

    private function loadFile($filepath) {
        $fileObject = new SplFileObject($filepath);

        foreach ($fileObject as $row) {

            trim($row);
            if (empty($row)) continue;
            $pos = strpos($row, ' ');
            $time = trim(substr($row, 0, $pos+1));
            $content = trim(substr($row, $pos+1));

            $this->notifications[$time] = $content;
        }
    }

}
