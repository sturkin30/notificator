<?php declare(strict_types=1);


final class Config
{
    private $config = [];

    public function __construct($envFilepath)
    {
        if (!file_exists($envFilepath)) {
            throw new \Exception('Invalid path to .env file');
        }

        $this->config = (new \josegonzalez\Dotenv\Loader($envFilepath))
              ->parse()
              ->toArray();
    }

    public function get($key) {
        return $this->config[$key] ?? null;
    }

}
