<?php declare(strict_types=1);


final class SendNotificationCommand
{

    const FROM = ["notification@turkin.tk", "TK Notification"];
    const SUBJECT = 'Notification';
    const TO = 'sturkin30@gmail.com';

    private $sendGrid;

    public function __construct($sendGridKey)
    {
        $this->sendGrid = new \SendGrid($sendGridKey);
    }

    public function execute($htmlText) {
        $email = new \SendGrid\Mail\Mail();
        [$fromEmail, $fromName] = self::FROM;
        $email->setFrom($fromEmail, $fromName);
        $email->setSubject(self::SUBJECT);
        $email->addTo(self::TO);
        $email->addContent(
            "text/html", $htmlText
        );

        try {
            $response = $this->sendGrid->send($email);
        } catch (Exception $e) {
            echo 'Caught exception: '. $e->getMessage() ."\n";
        }
    }

}
