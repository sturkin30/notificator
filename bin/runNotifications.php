<?php

require_once dirname(__FILE__) . '/../vendor/autoload.php';
require_once dirname(__FILE__) . '/../src/Config.php';
require_once dirname(__FILE__) . '/../src/SendNotificationCommand.php';
require_once dirname(__FILE__) . '/../src/Notifications.php';

$notifications = new Notifications(dirname(__FILE__) . '/../data/notifications.txt');
if ($htmlText = $notifications->getNowNotificationBody()) {
    send($htmlText);
}

function send($htmlText) {
    $envPath = dirname(__FILE__) . '/../.env';
    $config = new Config($envPath);
    $sendCommand = new SendNotificationCommand($config->get('SENDGRID_API_KEY'));
    $sendCommand->execute($htmlText);
}
