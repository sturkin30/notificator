<?php

require_once dirname(__FILE__) . '/../vendor/autoload.php';
require_once dirname(__FILE__) . '/../src/Config.php';
require_once dirname(__FILE__) . '/../src/SendNotificationCommand.php';

$envPath = dirname(__FILE__) . '/../.env';
$config = new Config($envPath);

$sendCommand = new SendNotificationCommand($config->get('SENDGRID_API_KEY'));

$sendCommand->execute('<strong>Its alive!</strong><br/> <i>from test</i>');

echo 'SENDED!!!' . "\n";
