<?php

require_once dirname(__FILE__) . '/../vendor/autoload.php';
require_once dirname(__FILE__) . '/../src/Config.php';

$envPath = dirname(__FILE__) . '/../.env';
$config = new Config($envPath);

var_dump($config->get('SENDGRID_API_KEY'));
